# Git Practice


Qué comando utilizaste en el paso 11?  
**Git reset --hard HEAD~1**
¿Por qué? 
**Así deshago el commit anterior perdiendo los cambios solamente en el working copy**

¿Qué comando o comandos utilizaste en el paso 12? 
**Git reflog para buscar el id y git reset --hard id** 
¿Por qué? 
**Primero busco el identificador del commit y luego lo restauro**

El merge del paso 13, ¿Causó algún conflicto? 
**No**
¿Por qué? 
**Porque las ramas estaban alineadas**

El merge del paso 19, ¿Causó algún conflicto? 
**Si**
¿Por qué? 
**Porque había cambios en el mismo archivos en ambas ramas**

El merge del paso 21, ¿Causó algún conflicto? 
**No**
¿Por qué? 
**El archivo está bien en todas las ramas**

¿Qué comando o comandos utilizaste en el paso 25? 
**git log --graph**

El merge del paso 26, ¿Podría ser fast forward? 
**Si**
¿Por qué? 
**Porque podría volver sin problema a todos los commit anteriores**

¿Qué comando o comandos utilizaste en el paso 27? 
**git reset HEAD~1**

¿Qué comando o comandos utilizaste en el paso 28? 
**git restore gitnuestro**

¿Qué comando o comandos utilizaste en el paso 29? 
**git branch -D title**

¿Qué comando o comandos utilizaste en el paso 30?
 **git reflog y git reset --hard id**

¿Qué comando o comandos usaste en el paso 32?
 **git reflog y git reset --hard id**
 
¿Qué comando o comandos usaste en el punto 33? 
**git reflog y git reset --hard id**
